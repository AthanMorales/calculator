/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class CalculatorDemo {
    public static void main(String args[]){
        Add add_1 = new Add(2,4);
        Substract sub_1 = new Substract(2,4);
        Multiply mul_1 = new Multiply(2,4);
        
        System.out.printf("The sum of 2,4 is %s\n", add_1.calculate());
        System.out.printf("The subtraction of 2,4 is %s\n", sub_1.calculate());
        System.out.printf("The multiplication of 2,4 is %s\n", mul_1.calculate());
    }
}
